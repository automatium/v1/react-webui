import React from "react";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import InboxIcon from "@material-ui/icons/Inbox";
import DraftsIcon from "@material-ui/icons/Drafts";
function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

const useStyles = makeStyles(theme => ({
  title: {
    flexGrow: 1
  }
}));

const Valves = () => {
  const classes = useStyles();

  let valves = [
    { name: "Valve 1", status: "Idle" },
    { name: "Valve 2", status: "Running" },
    { name: "Valve 3", status: "Idle" },
    { name: "Valve 4", status: "Idle" }
  ];

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Valves
          </Typography>
          {/* <Button color="inherit">Login</Button> */}
        </Toolbar>
      </AppBar>

      <List component="nav">
        {valves.map(valve => {
          return (
            <ListItem button>
              <ListItemText primary={valve.name} secondary={valve.status} />
            </ListItem>
          );
        })}
      </List>
    </div>
  );
};

export default Valves;
