import React from "react";
import logo from "./logo.svg";
import "typeface-roboto";
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import MenuIcon from "@material-ui/icons/Menu";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import Paper from "@material-ui/core/Paper";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  matchPath
} from "react-router-dom";
import _ from "lodash";

import RestoreIcon from "@material-ui/icons/Restore";
import FavoriteIcon from "@material-ui/icons/Favorite";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import HomeIcon from "@material-ui/icons/Home";
import WifiTetheringIcon from "@material-ui/icons/WifiTethering";
import OpacityIcon from "@material-ui/icons/Opacity";

import Valves from "./Valves";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
    // width: 50
  },
  menuButton: {
    marginRight: theme.spacing(2)
  }
}));

function App() {
  return (
    <Router>
      <RoutedApp />
    </Router>
  );
}

export default App;

let pages = [
  {
    name: "System",
    path: "/",
    icon: <HomeIcon />
  },
  {
    name: "Devices",
    path: "/devices",
    icon: <WifiTetheringIcon />
  },
  {
    name: "Valves",
    path: "/valves",
    icon: <OpacityIcon />
  }
];

const RoutedApp = () => {
  const classes = useStyles();
  const history = useHistory();

  console.log(history.location.pathname);

  let page = _.findLast(pages, page => {
    let match = matchPath(history.location.pathname, {
      path: page.path,
      exact: false,
      strict: false
    });
    return !!match;
  });

  return (
    <div className="bg-blue flex flex-col h-full">
      <div className="w-full max-w-2xl m-auto h-full flex flex-col">
        <Switch>
          <Route path="/devices">
            <AppBar position="static">
              <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="menu">
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  Devices
                </Typography>
                {/* <Button color="inherit">Login</Button> */}
              </Toolbar>
            </AppBar>

            <div>Hello, World!</div>
          </Route>
          <Route path="/valves">
            <Valves />
          </Route>
          <Route path="/">
            <AppBar position="static">
              <Toolbar>
                <IconButton edge="start" color="inherit" aria-label="menu">
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  Better Together Farm
                </Typography>
                {/* <Button color="inherit">Login</Button> */}
              </Toolbar>
            </AppBar>

            <div>Hello, World!</div>
          </Route>
        </Switch>

        <Paper elevation={6} className="mt-auto">
          <BottomNavigation
            value={page.path}
            onChange={(event, newValue) => {
              history.push(newValue);
            }}
            showLabels
            className={classes.root}
          >
            {pages.map(page => {
              return (
                <BottomNavigationAction
                  key={page.path}
                  label={page.name}
                  value={page.path}
                  icon={page.icon}
                />
              );
            })}
          </BottomNavigation>
        </Paper>
      </div>
    </div>
  );
};
